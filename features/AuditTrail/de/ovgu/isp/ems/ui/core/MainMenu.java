package de.ovgu.isp.ems.ui.core;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import de.ovgu.isp.ems.core.Localization;
import de.ovgu.isp.ems.db.Emp;
import de.ovgu.isp.ems.ui.Audit_details;
import de.ovgu.isp.ems.ui.UIFeature;

public class MainMenu {

	// Audit Trail Menu Entry:
	private JMenuItem jMenuItem5 = new JMenuItem();
	private JMenu btn_menu = new JMenu();
	private JMenuItem jMenuItem13 = new JMenuItem();
	// Audit Trail Menu Button:
	JButton jButton9 = new JButton();
	// JPanel:
	JPanel jPanel2 = new JPanel();
	UIFeature auditTrail = new UIFeature(messages.getString("audit_trail"), "/Images/Emp.png",
			new ActionListener() {
				public void actionPerformed(ActionEvent evt) {
					openAuditDialog(evt);
				}
			}, jButton9);
	
	/**
	 * this method add the feature AuditTrail to the arraylist in the main menu
	 */
	private void addUIFeatureToList() {
		original();
		featureList.add(auditTrail);
	}

	/**
	 * hook method in feature ListMenuUI
	 * 
	 * @param clickedItem
	 *            clicked list item on jlist
	 */
	private void compareWithClickedItemAction(String clickedItem) {
		original(clickedItem);
		if (auditTrail.getFeatureName().equals(clickedItem)) {
			openAuditDialog(null);
		}
	}

	/**
	 * this hook method initializes the audit trail menu entry in the top menu bar
	 */
	private void initComponents() {
		original();
		// Set Menu Entry for AuditTrails:
		jMenuItem5.setText(messages.getString("audit_trail"));
		jMenuItem5.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				openAuditDialog(evt);
			}
		});
		btn_menu.add(jMenuItem5);

		jMenuItem13.setText(messages.getString("add_user"));
		jMenuItem13.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				buttonActionUsers(evt);
			}
		});
		
		btn_menu.setText("Administration");
		btn_menu.add(jMenuItem13);

		jMenuBar1.add(btn_menu);

		pack();

	}

	/**
	 * this method writes a session logout to the database
	 */
	public void writeLogoutToAuditDetails() {
		original();
		// INSERT into AUDIT Table
		try {
			Localization _loc = new Localization();
			int value = Emp.empId;
			String reg = "insert into Audit (emp_id,date,status) values ('" + value + "','" + _loc.getTimeString()
					+ " / " + _loc.getDateString() + "','Logged out')";
			pst = conn.prepareStatement(reg);
			pst.execute();
			this.dispose();
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, e);
		} finally {
			try {
				rs.close();
				pst.close();
			} catch (Exception e) {

			}
		}
	}

	/**
	 * this method opens an Audit Details JFrame dialog
	 * 
	 * @param evt
	 */
	private void openAuditDialog(ActionEvent evt) {// GEN-FIRST:event_openAuditDialog
		Audit_details x = new Audit_details();
		x.setVisible(true);

	}

}