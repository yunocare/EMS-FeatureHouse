package de.ovgu.isp.ems.core;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.ResourceBundle;

//#ifdef ApplicationLogging
//@ import de.ovgu.isp.ems.logging.ApplicationLogger;
//@ import org.apache.logging.log4j.Logger;
//@ import de.ovgu.isp.ems.db.Emp;
//#endif

/**
 * 
 * @author fs
 *
 */
public class Localization {
	// mandatory <Feature:Localization>

	// #ifdef ApplicationLogging
	// @ org.apache.logging.log4j.Logger log =
	// @ new de.ovgu.isp.ems.logging.ApplicationLogger().getApplicationLogger();
	// #endif

	public Localization() {

	}

	/**
	 * 
	 * @return
	 */
	public String getTimeString() {
		Date d = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
		String timeString = sdf.format(d);
		// @ log.info("returning " + timeString + "as location specific time");
		return timeString;

	}

	/**
	 * 
	 * @return
	 */
	public String getDateString() {
		Date currentDate = GregorianCalendar.getInstance().getTime();
		DateFormat df = DateFormat.getDateInstance();
		String dateString = df.format(currentDate);
		// @ log.info("returning " + dateString + "as location specific date");

		return dateString;

	}

	/**
	 * 
	 * @return Resource Bundle in current configuration
	 */
	public ResourceBundle getBundle() {
		return ResourceBundle.getBundle("MessagesBundle", getCurrentLocale());

	}

	// <Feature:Localization>

}
