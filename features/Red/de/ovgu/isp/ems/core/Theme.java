package de.ovgu.isp.ems.core;

import java.awt.Color;

public class Theme {
	
	//<Feature:Red>
	Color _color = Color.RED;
	
	/**
	 * 
	 * @return theme color
	 */
	public Color getColor() {
		return _color;
	}
	
	//</Feature:Red>
}