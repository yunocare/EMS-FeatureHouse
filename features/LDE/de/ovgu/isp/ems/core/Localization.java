package de.ovgu.isp.ems.core;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.ResourceBundle;

/**
 * 
 * @author fs
 *
 */
public class Localization {
	/**
	 * 
	 * @return
	 */
	public String getCountrySpecificDate() {
		Calendar cal = new GregorianCalendar();
		int month = cal.get(Calendar.MONTH);
		int year = cal.get(Calendar.YEAR);
		int day = cal.get(Calendar.DAY_OF_MONTH);
		return " | " + day + "." + (month + 1) + "." + year + " | ";

	}

	/**
	 * 
	 * @return locale germany
	 */
	public Locale getCurrentLocale() {
		Locale _locale = new Locale("de", "DE");
		return _locale;
	}

}
