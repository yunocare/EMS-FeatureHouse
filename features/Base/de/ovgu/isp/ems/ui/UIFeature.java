package de.ovgu.isp.ems.ui;

import javax.swing.JButton;
import java.awt.event.ActionListener;

/**
 * 
 * @author fs
 *
 */
public class UIFeature {

	String featureName;
	String featureImagePath;
	ActionListener featureListener;
	JButton featureButton;

	/*
	 * UIFeature constructors:
	 */

	/**
	 * 
	 * @param _featureName
	 * @param _imagePath
	 * @param _featureListener
	 * @param _featureButton
	 */
	public UIFeature(String _featureName, String _imagePath, ActionListener _featureListener, JButton _featureButton) {
		this.featureName = _featureName;
		this.featureImagePath = _imagePath;
		this.featureListener = _featureListener;
		this.featureButton = _featureButton;
	}

	/*
	 * UI Feature getter:
	 */

	/**
	 * 
	 * @return ui feature name
	 */
	public String getFeatureName() {
		return featureName;
	}

	/**
	 * 
	 * @return path to image
	 */
	public String getFeatureImagePath() {
		return featureImagePath;
	}

	/**
	 * 
	 * @return
	 */
	public ActionListener getFeatureListener() {
		return featureListener;
	}

	/**
	 * 
	 * @return
	 */
	public JButton getFeatureButton() {
		return featureButton;
	}
}