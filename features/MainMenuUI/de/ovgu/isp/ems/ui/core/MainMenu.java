package de.ovgu.isp.ems.ui.core;

import java.awt.Dimension;
import java.awt.Toolkit;

public class MainMenu {

	/*
	 * Main Menu JFrame configuration in ButtonMenuUI:
	 */
	Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
	int window_height = (int)(screenSize.getHeight());
	int window_width = (int)(screenSize.getWidth())/2;
//	int window_height = 800;
//	int window_width = 1200;
	
	/*
	 * ButtonMenuUI configuration:
	 */
	int _buttonhight = 180;
	int _buttonwidth = 300;
	int _buttonY = (window_width / 2 - _buttonwidth); // center
	int _buttonYoffset = 0;
	int _buttonX = 300;
	
	/*
	 * ListMenuUI configuration:
	 */
	
	int _xlength = 300;
	int _ylength = 450;

	int _xbound = 300;//(window_width/2) - (_xlength /2);
	int _ybound = (window_height/2) +(_ylength/2);

	
}