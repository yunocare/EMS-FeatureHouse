import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import de.ovgu.isp.ems.ui.UIFeature;

public class MainMenu {

	private void initializeAllowanceButtonUI(){
		// Initializing featureList with standard ui components:
		UIFeature empDeductions = new UIFeature(messages.getString("deduction"), "/Images/Deduction.png",
				new ActionListener() {
					public void actionPerformed(ActionEvent evt) {
						buttonActionEmployeeDeduction();
					}
				}, jButton3);

		UIFeature allowance = new UIFeature(messages.getString("allowance"), "/Images/Allowance.png",
				new ActionListener() {
					public void actionPerformed(ActionEvent evt) {
						buttonActionAllowance();
					}
				}, jButton2);

		UIFeature payment = new UIFeature(messages.getString("payment"), "/Images/Payment.png", new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				buttonActionSearchSalarySlip();
			}
		}, jButton7);

		featureList.add(empDeductions);
		featureList.add(allowance);
		featureList.add(payment);
	}
	/**
	 * Button: Allowance
	 * 
	 * @param evt
	 */
	private void buttonActionAllowance() {// GEN-FIRST:event_buttonActionAllowance
		Allowance x = new Allowance();
		x.setVisible(true);
	}// GEN-LAST:event_buttonActionAllowance
	
	/**
	 * Button: Employee Deduction
	 * 
	 * @param evt
	 */
	private void buttonActionEmployeeDeduction() {// GEN-FIRST:event_buttonActionEmployeeDeduction

		employeeDeductions x = new employeeDeductions();
		x.setVisible(true);

	}// GEN-LAST:event_buttonActionEmployeeDeduction

	/**
	 * Button: Search Emp Salary Slip
	 * 
	 * @param evt
	 */
	private void buttonActionSearchSalarySlip() {// GEN-FIRST:event_buttonActionSearchSalarySlip
		// TODO: rename to buttonActionPayment
		System.out.println("Button Search Emp Salary Slip clicked.");

		searchEmpSalarySlip x = new searchEmpSalarySlip();
		x.setVisible(true);

	}// GEN-LAST:event_buttonActionSearchSalarySlip
}