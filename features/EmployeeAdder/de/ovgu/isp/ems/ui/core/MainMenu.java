package de.ovgu.isp.ems.ui.core;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import java.awt.event.ActionEvent;
import de.ovgu.isp.ems.ui.UIFeature;
import de.ovgu.isp.ems.ui.addEmployee;

public class MainMenu {

	JButton jButton1 = new JButton();

	UIFeature employeeAdder = new UIFeature(messages.getString("emp_manager"), "/Images/Add_Employee.png",
			new ActionListener() {
				public void actionPerformed(ActionEvent evt) {
					add_employeeActionPerformed(evt);
				}
			}, jButton1);

	/**
	 * Button:
	 * 
	 * @param evt
	 */
	private void add_employeeActionPerformed(ActionEvent evt) {// GEN-FIRST:event_add_employeeActionPerformed
		if (evt == null) {
			System.out.println("Click from list menu");
			JFrame f = new addEmployee();
			// TODO: open jframe inside jpanel - outstanding!
		} else {
			addEmployee x = new addEmployee();
			// #ifdef ApplicationLogging
			// @ log.info("add employee frame opened by user " + Emp.empId);
			// #endif
			x.setVisible(true);
		}

	}
	
	/**
	 * Button: Users
	 * 
	 * @param evt
	 */
	private void buttonActionUsers(ActionEvent evt) {// GEN-FIRST:event_buttonActionUsers
		// TODO: extract to feature -> users is not part of feature Base
		users x = new users();
		x.setVisible(true);
	}

	/**
	 * this method add the feature EmployeeAdder to the arraylist in the main menu
	 */
	private void addUIFeatureToList() {
		original();
		featureList.add(employeeAdder);
	}

	/**
	 * hook method in feature ListMenuUI
	 * 
	 * @param clickedItem
	 *            clicked list item on jlist
	 */
	private void compareWithClickedItemAction(String clickedItem) {
		original(clickedItem);
		if (employeeAdder.getFeatureName().equals(clickedItem)) {
			add_employeeActionPerformed(null);
		}
	}

	private void initComponents() {
		original();
		// Initialize EMPLOYEE Menu (top left in menu bar)
		jMenu1.setText(messages.getString("employee"));

		// Add Employee Registration entry to EMPLOYEE menu
		jMenuItem1.setText(messages.getString("emp_registration"));
		jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				add_employeeActionPerformed(evt);
			}
		});
		// Use JMenuBar
		jMenu1.add(jMenuItem1);
		jMenuBar1.add(jMenu1);

	}

}
