package de.ovgu.isp.ems.core;

public class AccessControl {

	//<Feature:TwoFA>
	
	// Two factors (user name + password) are needed to perform a login

	/**
	 * 
	 * @return login query for user name + password combination
	 */
	public String getLoginQuery() {
		return "select id,username,password,division from Users Where (username =? and password =?)";
	}

	//</Feature:TwoFA>
}