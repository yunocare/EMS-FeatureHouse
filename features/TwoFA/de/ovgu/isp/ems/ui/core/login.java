package de.ovgu.isp.ems.ui.core;

public class login {

	// <Feature:TwoFA>
	/**
	 * If the two factor authentication feature is selected, any user has full admin
	 * rights.
	 * 
	 * @return user role equals admin by default
	 */

	public String getSelectedUserRole() {
		// #ifdef ApplicationLogging
		// @ log.info("Configuration with two factor authentication, provides admin" +
		// @ "rights to user.");
		// #endif
		return "Admin";
	}

	// </Feature:TwoFA>

}