package de.ovgu.isp.ems.ui.core;

import de.ovgu.isp.ems.ui.UIFeature;
import de.ovgu.isp.ems.ui.searchEmployee;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MainMenu {

	boolean lock = false;
	UIFeature searchEmployee = new UIFeature(messages.getString("search"), "/Images/Search.png", new ActionListener() {
		public void actionPerformed(ActionEvent evt) {
			openSearchEmployeeDialog(evt);
		}
	}, jButton4);

	/**
	 * this methods opens a new JFrame dialog Main Menu Link: Search Employee
	 * 
	 * @param evt
	 */
	private void openSearchEmployeeDialog(ActionEvent evt) {// GEN-FIRST:event_openSearchEmployeeDialog
		//TODO: LOCK
		if (lock) {
			//TODO: implement windowclose operation in searchEmployee

		} else {
			searchEmployee e = new searchEmployee();
			e.setVisible(true);
		}
	}

	/**
	 * hook method in feature ListMenuUI
	 * 
	 * @param clickedItem
	 *            clicked list item on jlist
	 */
	private void compareWithClickedItemAction(String clickedItem) {
		original(clickedItem);
		if (searchEmployee.getFeatureName().equals(clickedItem)) {
			openSearchEmployeeDialog(null);
		}
	}

	/**
	 * this method add the feature salaryUpdater to the arraylist in the main menu
	 */
	private void addUIFeatureToList() {

		featureList.add(searchEmployee);
	}

}